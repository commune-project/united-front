import { IVNode, IVNodeProps, IVText, TVTree, isVNode, isVText, TEventHandler } from "./vnode/vnode"
import { isEventProp } from "./vnode/create-element"
import { updateNodes } from "./vnode/diff"


function extractEventName(name: string): string {
  return name.slice(2).toLowerCase();
}

function h(tagName: string, props: any, ...children: (IVNode | string)[]): IVNode {
  const realChildren = children.map((value: (IVNode | string)): TVTree => {
    if (typeof value === "string") {
      return {
        type: "vtext",
        text: value
      }
    } else {
      return value
    }
  })

  const events: { [name: string]: TEventHandler } = {}
  for (const propName in props) {
    if (isEventProp(propName)) {
      events[extractEventName(propName)] = props[propName]
    }
  }

  return Object.assign(
    Object.create(null),
    {
      type: "vnode",
      tagName,
      props,
      children: realChildren,
      events
    })
}

const test1 = [
  h("div", { className: "qunimade" },
    h("textarea", {},
      "qunimade")),
  h("p", {}, "nimasile")
]
updateNodes(document.body, test1)

console.log("tested 1")

const test2 = [
  h("div", { className: "woshuoguome" },
    h("textarea", {},
      "doushabi")),
  h("p", {
    onClick(e: Event): void {
      e.preventDefault()
      alert("nimasland!")
    }
  }, "hama")
]
updateNodes(document.body, test2, test1)
console.log("tested 2")
