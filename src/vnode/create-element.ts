import { IVNode, IVNodeProps, IVText, TVTree, isVNode, isVText, TEventHandler } from "./vnode"

function createElementInDocument(vnode: IVNode): Element {
  if (vnode.namespace) {
    return document.createElementNS(vnode.namespace, vnode.tagName)
  } else {
    return document.createElement(vnode.tagName)
  }
}


function createElement(vtree: IVNode): Element
function createElement(vtree: IVText): Text
function createElement(vtree: TVTree): Node
function createElement(vtree: TVTree): Node {
  if (isVText(vtree)) {
    vtree.dom = document.createTextNode(vtree.text)
    return vtree.dom
  } else {
    vtree.dom = createElementInDocument(vtree)
    setProps(vtree.dom, vtree.props)
    addEventListeners(vtree)
    createChildrenElement(vtree)
    return vtree.dom
  }
}

function createElements($elem: Element, vtrees: TVTree[]): void{
  vtrees.map(createElement).forEach($elem.appendChild.bind($elem))
}


function createChildrenElement(vnode: IVNode): void {
  if (vnode.dom) {
    createElements(vnode.dom, vnode.children)
  }
}

// Properties

function setBooleanProp($target: Element & any, name: string, value: boolean): void {
  if (value) {
    $target.setAttribute(name, "");
    $target[name] = true;
  } else {
    $target[name] = false;
  }
}

function removeBooleanProp($target: Element & any, name: string): void {
  $target.removeAttribute(name);
  $target[name] = false;
}

function isCustomProp(name: string) {
  return isEventProp(name)
}

function setProp($target: Element, name: string, value: string | boolean) {
  if (isCustomProp(name)) {
    return
  } else if (typeof value === 'boolean') {
    setBooleanProp($target, name, value)
  } else if (name === 'className') {
    $target.setAttribute('class', value)
  } else {
    $target.setAttribute(name, value)
  }
}

function removeProp($target: Element, name: string, value: string | boolean) {
  if (isCustomProp(name)) {
    return
  } else if (typeof value === 'boolean') {
    removeBooleanProp($target, name);
  } else if (name === 'className') {
    $target.removeAttribute('class');
  } else {
    $target.removeAttribute(name);
  }
}

function setProps($target: Element, props: IVNodeProps) {
  Object.keys(props).forEach(name => {
    setProp($target, name, props[name]);
  });
}

function updateProp($target: Element, name: string, newVal: string | boolean, oldVal: string | boolean) {
  if (!newVal) {
    removeProp($target, name, oldVal);
  } else if (!oldVal || newVal !== oldVal) {
    setProp($target, name, newVal);
  }
}

function updateProps($target: Element, newProps: IVNodeProps, oldProps: IVNodeProps = {}) {
  const props = Object.assign({}, newProps, oldProps);
  Object.keys(props).forEach(name => {
    updateProp($target, name, newProps[name], oldProps[name]);
  });
}

// Events

function isEventProp(name: string): boolean {
  return name.substr(0, 2) === "on"
}

function addEventListeners(vnode: IVNode) {
  for (const eventName in vnode.events) {
    vnode.dom?.addEventListener(eventName, vnode.events[eventName])
  }
}

function removeEventListeners(vnode: IVNode): void {
  for (const eventName in vnode.events) {
    vnode.dom?.removeEventListener(eventName, vnode.events[eventName])
  }
}

export { createElement, createElements, updateProps, addEventListeners, removeEventListeners, isEventProp }