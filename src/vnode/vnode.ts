interface IVNodeProps {
  [id: string]: string | boolean
}

type TEventHandler = (e: Event)=>void

interface IVNode {
  type: 'vnode'
  tagName: string
  props: IVNodeProps
  children: TVTree[]
  dom?: Element
  namespace?: string
  events: {
    [name: string]: TEventHandler
  }
}

interface IVText {
  type: 'vtext'
  text: string
  dom?: Text
}

type TVTree = IVNode | IVText

function isVNode(x: TVTree): x is IVNode {
  return x.type === "vnode"
}

function isVText(x: TVTree): x is IVText {
  return x.type === "vtext"
}

export { IVNode, IVNodeProps, IVText, TVTree, isVNode, isVText, TEventHandler}