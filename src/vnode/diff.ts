import { IVNode, IVNodeProps, IVText, TVTree, isVNode, isVText, TEventHandler } from "./vnode"
import { createElements, createElement, removeEventListeners, updateProps, addEventListeners } from "./create-element"

function removeChild($parent: Element, vtree: TVTree): void {
  $parent.removeChild(vtree.dom as Node)
}

function removeNodes($parent: Element, vtrees: TVTree[]): void {
  vtrees.forEach((vtree)=>{
    if (isVNode(vtree)) {
      removeEventListeners(vtree)
    }
    removeChild($parent, vtree)
  })
}


function updateNodes($parent: Element, newNodes?: TVTree[], oldNodes?: TVTree[]): void {
  if (oldNodes === newNodes || !newNodes && !oldNodes) {
    return
  } else if (!oldNodes || oldNodes.length === 0) {
    // No old nodes, create new ones.
    createElements($parent, newNodes as TVTree[])
  } else if (!newNodes || newNodes.length === 0) {
    // No new nodes, delete all
    removeNodes($parent, oldNodes as TVTree[])
  } else if (newNodes.length !== oldNodes.length) {
    // TODO Optimize it
    removeNodes($parent, oldNodes)
    createElements($parent, newNodes)
  } else {
    for (let i=0; i<newNodes.length; i++) {
      if (isNodeItselfChanged(oldNodes[i], newNodes[i])) {
        updateNode($parent, newNodes[i], oldNodes[i])
      } else {
        // currentNode itself is not changed, update children only.
        const currentNode = oldNodes[i]
        if (isVNode(currentNode) && currentNode.dom) {
          const newNode = newNodes[i] as IVNode
          removeEventListeners(currentNode)
          currentNode.events = newNode.events
          addEventListeners(currentNode)
          updateProps(currentNode.dom, newNode.props, currentNode.props)
          updateNodes(currentNode.dom, newNode.children, currentNode.children)
        }
      }
    }
  }
}

function updateNode($parent: Element, newNode: TVTree, oldNode: TVTree): TVTree {
  $parent.replaceChild(createElement(newNode), oldNode.dom as Element)
  return newNode
}

function isNodeItselfChanged(oldNode: TVTree, newNode: TVTree): boolean {
  if (oldNode === newNode) {
    return false
  } else if (oldNode.type !== newNode.type) {
    return true
  } else if (isVNode(newNode)) {
    if ((oldNode as IVNode).tagName !== newNode. tagName) {
      return true
    } else {
      return false
    }
  } else if (isVText(newNode)) {
    return newNode.text !== (oldNode as IVText).text
  } else {
    return false
  }
}

export { updateNodes }